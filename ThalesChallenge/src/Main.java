import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {
		String line;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			int registersNumber = Integer.parseInt(br.readLine());
			for (int i = 0; i < registersNumber; i++) {
				line = br.readLine();
				Parser.ParseFile(line, i + 1);
			}
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
		}
	}
}
