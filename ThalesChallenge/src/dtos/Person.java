package dtos;

public class Person {

	private Integer index;
	private Integer scanId;
	private String documentType;
	private String issuingCountry;
	private String lastName;
	private String firstName;
	private Integer documentNumber;
	private String nationality;
	private String birthDate;
	private String expiryDate;

	public Person() {

	}

	public Person(Integer index, int scanId, String documentType, String issuingCountry, String lastName,
			String firstName, int documentNumber, String nationality, String birthDate, String expiryDate) {
		super();
		this.index = index;
		this.scanId = scanId;
		this.documentType = documentType;
		this.issuingCountry = issuingCountry;
		this.lastName = lastName;
		this.firstName = firstName;
		this.documentNumber = documentNumber;
		this.nationality = nationality;
		this.birthDate = birthDate;
		this.expiryDate = expiryDate;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getScanId() {
		return scanId;
	}

	public void setScanId(Integer scanId) {
		this.scanId = scanId;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getIssuingCountry() {
		return issuingCountry;
	}

	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Integer getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(Integer documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "Person [index=" + index + ", scanId=" + scanId + ", documentType=" + documentType + ", issuingCountry="
				+ issuingCountry + ", lastName=" + lastName + ", firstName=" + firstName + ", documentNumber="
				+ documentNumber + ", nationality=" + nationality + ", birthDate=" + birthDate + ", expiryDate="
				+ expiryDate + "]";
	}

}
