package utils;

public class Constants {

	//
	public static final Integer NUMBER_OF_PARAMTERS = 9;
	public static final String ARGUMENT_SEPARATOR = ",";
	public static final char LINE_DELIMITER = '\n';
	public static final char LINE_RETURN = '\r';

	// DATES FORMATS
	public static final String DATE_FORMAT_NOW = "yyMMdd";
	public static final String DATE_FORMAT_DOCUMENT = "yyMMdd";
	public static final String DATE_VALIDATION_PATTERN = "[0-9]{6}";

	// ERROR MESSAGES
	public static final String ERROR_MESSAGE_SAME_PERSON = "same person";
	public static final String ERROR_MESSAGE_EXPIRED = "expired";
	public static final String ERROR_MESSAGE_NON_ACCEPTED_COUNTRY = "non-accepted country";
	public static final String ERROR_MESSAGE_INVALID_DATE = "invalid date";
	public static final String ERROR_MESSAGE_INCORRECT_PARAMETER = "incorrect parameter";
	public static final String ERROR_MESSAGE_NUMBER_OF_ARGUMENTS = "incorrect numbers of arguments";

	// COUNTRIES

	public static final String COUNTRY_DEF_SPAIN = "ESP";
	public static final String COUNTRY_DEF_FRANCE = "FRA";
	public static final String COUNTRY_DEF_PORTUGAL = "POR";
	public static final String COUNTRY_DEF_ANDORRA = "AND";
	public static final String COUNTRY_DEF_MORROCO = "MOR";

}
