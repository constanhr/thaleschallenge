package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dtos.Person;

public class Utils {

	public static String Now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}

	public static boolean CheckParameters(String[] person) {
		boolean correct = false;

		if (person.length == Constants.NUMBER_OF_PARAMTERS) {
			correct = true;
		}

		return correct;
	}

	public static boolean CheckDocumentType(String document) {
		boolean correct = false;
		switch (document) {
		case "P":
		case "DL":
		case "ID":
			correct = true;
			break;
		}
		return correct;
	}

	public static boolean DateValidation(String expiryDate, String birthDate, String now) {
		boolean correct = false;
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_DOCUMENT);
		formatter.setLenient(false);
		try {
			Date date = formatter.parse(expiryDate);
			date = formatter.parse(birthDate);
			if (Integer.parseInt(birthDate) < Integer.parseInt(now)) {
				correct = true;
			}
		} catch (ParseException e) {
			// e.printStackTrace();
		}
		return correct;
	}

	public static boolean CheckExpiryDate(String expiryDate, String now) {
		boolean correct = false;
		SimpleDateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT_DOCUMENT);
		try {
			Date date = formatter.parse(expiryDate);
			if (Integer.parseInt(expiryDate) > Integer.parseInt(now)) {
				correct = true;
			}
		} catch (ParseException e) {
			// e.printStackTrace();
		}
		return correct;
	}

	public static boolean CheckSpainAcceptedCountrys(String country) {
		boolean correct = false;
		switch (country) {
		case Constants.COUNTRY_DEF_SPAIN:
		case Constants.COUNTRY_DEF_FRANCE:
		case Constants.COUNTRY_DEF_PORTUGAL:
		case Constants.COUNTRY_DEF_ANDORRA:
		case Constants.COUNTRY_DEF_MORROCO:
			correct = true;
			break;
		}
		return correct;
	}

	public static boolean AreSamePersons(Person p1, Person p2) {
		boolean equals = false;
		if ((Integer.compare(p1.getDocumentNumber(), p2.getDocumentNumber()) == 0
				&& p1.getDocumentType().equals(p2.getDocumentType())
				&& p1.getIssuingCountry().equals(p2.getIssuingCountry()))
				|| (p1.getFirstName().equals(p2.getFirstName()) && p1.getLastName().equals(p2.getLastName())
						&& p1.getNationality().equals(p2.getNationality())
						&& p1.getBirthDate().equals(p2.getBirthDate()))) {
			equals = true;
		}
		return equals;
	}

}
