import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import dtos.Person;
import utils.Constants;
import utils.Utils;

public class Parser {

	private static String toOutputList;
	private static ArrayList<Person> correctPersonList = new ArrayList<Person>();

	public static void ParseFile(String line, Integer index) throws IOException {
		String now = Utils.Now();
		String[] sPerson = line.toString().split(Constants.ARGUMENT_SEPARATOR);
		if (index > 0) {
			CheckPerson(sPerson, index, now);
		}
	}

	public static void CheckPerson(String[] sPerson, Integer index, String now) throws IOException {

		if (Utils.CheckParameters(sPerson)) {
			try {
				Person person = new Person(index, Integer.parseInt(sPerson[0]), sPerson[1], sPerson[2].substring(0, 3),
						sPerson[3], sPerson[4], Integer.parseInt(sPerson[5]), sPerson[6].substring(0, 3), sPerson[7],
						sPerson[8]);
				if (ValidationConditions(person, index, now)) {
					correctPersonList.add(person);
				}
			} catch (Exception e) {
				toOutputList = (index.toString() + Constants.ARGUMENT_SEPARATOR
						+ Constants.ERROR_MESSAGE_INCORRECT_PARAMETER);
				System.out.println(toOutputList);
			}
		} else {
			toOutputList = (index.toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_NUMBER_OF_ARGUMENTS);
			System.out.println(toOutputList);
		}

	}

	public static boolean ValidationConditions(Person person, Integer index, String now) throws IOException {
		boolean correct = true;
		if (person.getIndex() != index) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_INCORRECT_PARAMETER);
			System.out.println(toOutputList);
		} else if (!Utils.CheckDocumentType(person.getDocumentType())) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_INCORRECT_PARAMETER);
			System.out.println(toOutputList);
		} else if (!Pattern.matches(Constants.DATE_VALIDATION_PATTERN, person.getBirthDate())
				|| !Pattern.matches(Constants.DATE_VALIDATION_PATTERN, person.getExpiryDate())) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_INCORRECT_PARAMETER);
			System.out.println(toOutputList);
		} else if (!Utils.DateValidation(person.getExpiryDate(), person.getBirthDate(), now)) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_INVALID_DATE);
			System.out.println(toOutputList);
		} else if (!Utils.CheckExpiryDate(person.getExpiryDate(), now)) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_EXPIRED);
			System.out.println(toOutputList);
		} else if (!Utils.CheckSpainAcceptedCountrys(person.getIssuingCountry())) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_NON_ACCEPTED_COUNTRY);
			System.out.println(toOutputList);
		} else if (CheckExistsPerson(person)) {
			correct = false;
			toOutputList = (person.getIndex().toString() + Constants.ARGUMENT_SEPARATOR
					+ Constants.ERROR_MESSAGE_SAME_PERSON);
			System.out.println(toOutputList);
		}
		return correct;
	}

	public static boolean CheckExistsPerson(Person person) {
		boolean exists = false;
		for (Person personInList : correctPersonList) {
			if (Utils.AreSamePersons(personInList, person)) {
				exists = true;
				break;
			}
		}
		return exists;
	}
}
